#ifndef _BMP_H_
#define _BMP_H_
#include <inttypes.h>
#include <stdio.h>
#include "image.h"

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_FILE,
  READ_INVALID_POINTER,
  READ_NO_MEM
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_INVALID_IMAGE,
  WRITE_INVALID_FILE,
  WRITE_INVALID_POINTER
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const img );

#endif
