#ifndef _IMAGE_H_
#define _IMAGE_H_
#include<inttypes.h>
#include <stdint.h>

struct pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
};

struct image{
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};

extern const struct image wrong_image;

struct image image_init(uint64_t width, uint64_t height);

void image_destroy(struct image *image);

struct pixel* pixel_at(struct image img, uint64_t x, uint64_t y);
#endif
