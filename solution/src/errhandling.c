#include "errhandling.h"
#include "BMP.h"
#include "fileoc.h"

static const char *open_errs[] = {[OPEN_ERROR] = "OPEN_ERROR"};

static const char *close_errs[] = {[CLOSE_ERROR] = "CLOSE_ERROR"};

static const char *read_errs[] = {[READ_INVALID_FILE] = "READ_INVALID_FILE",
[READ_INVALID_POINTER] = "READ_INVALID_POINTER",
[READ_INVALID_HEADER] = "READ_INVALID_HEADER",
[READ_INVALID_SIGNATURE] = "READ_INVALID_SIGNATURE",
[READ_INVALID_BITS] = "READ_INVALID_BITS",
[READ_NO_MEM] = "READ_NO_MEM"};

static const char *write_errs[] = {[WRITE_INVALID_FILE] = "WRITE_INVALID_FILE",
[WRITE_INVALID_POINTER] = "WRITE_INVALID_POINTER",
[WRITE_INVALID_IMAGE] = "WRITE_INVALID_IMAGE",
[WRITE_ERROR] = "WRITE_ERROR"};

const char* get_file_open_err(enum open_status status){
	return open_errs[status];
}

const char* get_file_close_err(enum close_status status){
	return close_errs[status];
}

const char* get_BMP_read_err(enum read_status status){
	return read_errs[status];
}

const char* get_BMP_write_err(enum write_status status){
	return write_errs[status];
}
