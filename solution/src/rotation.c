#include "rotation.h"
#include "image.h"
#include <stdint.h>

static void rotation(const struct image old_image, struct image new_image);

struct image rotate(const struct image source){
	struct image img = image_init(source.height, source.width);
	if(!img.data)
		return wrong_image;	
	rotation(source, img);
	return img;
}

static void rotation(const struct image old_image, struct image new_image){
	for(uint64_t y = 0; y < old_image.height; y++){
		for(uint64_t x = 0; x < old_image.width; x++){
			struct pixel *pixel = pixel_at(old_image, x, y);
			const uint64_t new_x = y;
			const uint64_t new_y = new_image.height - x - 1;
			*pixel_at(new_image, new_x, new_y) = *pixel;
		}
	}
}
