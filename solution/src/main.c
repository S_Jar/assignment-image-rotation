#include "photoshop.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc != 3 || !argv[1] || !argv[2]){
	    printf("usage ./%s <source-image> <transformed-image>", argv[0]);
	    return 1;
    }
    if(rotate_BMP_image(argv[1], argv[2]) != 0){
	printf("Error while rotating");
    	return 1;
    }
    printf("Success");
    return 0;
}

