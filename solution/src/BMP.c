#include "BMP.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>

#define BM 19778;
#define OFFSET 54
#define INFO_HEADER_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define MAX_HEIGHT UINT32_MAX
#define MAX_WIDTH UINT32_MAX

struct __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

static bool build_header(uint32_t width, uint32_t height,
	      const struct pixel* data, struct bmp_header *header);

static uint8_t calculate_padding(uint32_t width);

static bool check_type(struct bmp_header header);

static bool read_pixels(FILE *file, uint32_t offset, struct image img);

static bool write_header(FILE *file, struct bmp_header* header);

static bool write_pixels(FILE *file,uint32_t offset, struct image img);

enum read_status from_bmp(FILE *in, struct image *img){
	if(!in)
		return READ_INVALID_FILE;
	if(!img)
		return READ_INVALID_POINTER;
	struct bmp_header header = {0};
	if(!fread(&header, sizeof(struct bmp_header), 1, in))
		return READ_INVALID_HEADER;
	if(!check_type(header))
		return READ_INVALID_SIGNATURE;
	const uint32_t height = header.biHeight;
	const uint32_t width = header.biWidth;
	const uint32_t offset = header.bOffBits;
	*img = image_init(width, height);
	if(!img->data){
		return READ_NO_MEM;
	}
	if(!read_pixels(in, offset, *img))
		return READ_INVALID_BITS;
	return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image img){
	if(!out)
		return WRITE_INVALID_FILE;
	const uint32_t height = img.height;
	const uint32_t width = img.width;
	if( img.height > MAX_HEIGHT  || img.width > MAX_WIDTH || !img.data )
		return WRITE_INVALID_IMAGE;
	struct pixel* data = img.data;
	struct bmp_header header = {0};
	if(!build_header(width, height, data, &header))
		return WRITE_INVALID_IMAGE;
	if(!write_header(out, &header)){
		return WRITE_ERROR;
	}
	if(!write_pixels(out, header.bOffBits, img)){
		return WRITE_ERROR;
	}
	return WRITE_OK;
}

static bool build_header(uint32_t width, uint32_t height,
	      const struct pixel* data, struct bmp_header *header){
	if(!data || width == 0 || height == 0|| !header)
		return false;
	header->bfType = BM;
	const uint32_t sizeWithoutPadding = sizeof(struct pixel)*width*height;
	const uint32_t sizeOfPadding = calculate_padding(width)*height;
	header->biSizeImage = sizeWithoutPadding + sizeOfPadding;
	header->bfileSize = sizeof(struct bmp_header) + header->biSizeImage;
	header->bOffBits = OFFSET;
	header->biSize = INFO_HEADER_SIZE;
	header->biWidth = width;
	header->biHeight = height;
	header->biPlanes = PLANES;
	header->biBitCount = BIT_COUNT;
	header->biCompression = COMPRESSION;
	return true;
}

static uint8_t  calculate_padding(uint32_t width){
	return (4 - (sizeof(struct pixel)*width) % 4)%4;
}
static bool check_type(struct bmp_header header){	
	return header.bfType == BM;
}

static bool read_pixels(FILE *file, uint32_t offset, struct image img){
	const uint32_t width = img.width;
	const uint32_t height = img.height;
	const uint8_t padding = calculate_padding(width);
	if(fseek(file, offset, SEEK_SET) < 0)
		return false;
	for(uint32_t i = height; i > 0; --i){
		if(fread(pixel_at(img, 0, i - 1), sizeof(struct pixel), width, file) < width)
			return false;
		if(fseek(file, padding, SEEK_CUR) < 0)
			return false;
	}
	return true;
}

static bool write_header(FILE *file, struct bmp_header* header){
	return fwrite(header, sizeof(struct bmp_header), 1, file);
}

static bool write_pixels(FILE *file,uint32_t offset, struct image img){
	const uint32_t width = img.width;
	const uint32_t height = img.height;
	if(fseek(file, offset, SEEK_SET) < 0)
		return false;
	const uint8_t padding = calculate_padding(width);
	const uint8_t zero[3] = {0,0,0};
	for(uint32_t i = height; i > 0; --i){
		if(fwrite(pixel_at(img, 0, i - 1), sizeof(struct pixel), width, file) < width)
			return false;
		if(fwrite(&zero, 1, padding, file) < padding)
			return false;
	}
	return true;
}
