#include "image.h"
#include <malloc.h>
#include <stdint.h>

const struct image wrong_image = {
	.width = 0,
	.height = 0,
	.data = NULL
};

static uint64_t calculate_coordinates(uint64_t x, uint64_t y, uint64_t width);

struct image image_init(uint64_t width, uint64_t height){
	struct image img = {0};
	img.width = width;
	img.height = height;
	img.data = malloc (sizeof(struct pixel)*width*height);
	if(!img.data)
		return wrong_image;
	return img;
}

void image_destroy(struct image *image){
	if(image->data)
		free(image->data);
	image->height = 0;
	image->width = 0;
	image->data = NULL;
}

struct pixel* pixel_at(struct image img, uint64_t x, uint64_t y){
	if(img.data)
		return &img.data[calculate_coordinates(x, y, img.width)];
	return NULL;
}

static uint64_t calculate_coordinates(uint64_t x, uint64_t y, uint64_t width){
	return x + y * width;
}
