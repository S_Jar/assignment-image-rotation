#include "photoshop.h"

#include "errhandling.h"
#include "fileoc.h"
#include "rotation.h"

static void print_error(const char* err);

int rotate_BMP_image(char* source, char* dest){
    enum open_status o_st;
    enum close_status c_st;
    enum read_status r_st;
    enum write_status w_st;
    FILE* old_image = {0};
    o_st = open_for_read(source, &old_image); 
    if(o_st != OPEN_OK){
	    print_error(get_file_open_err(o_st));
	    return 2;
    }
    struct image image = {0};
    r_st = from_bmp(old_image, &image);
    if(r_st != READ_OK){
	    print_error(get_BMP_read_err(r_st));
	    image_destroy(&image);
	    return 3;
    }
    c_st = close(old_image);
    if(c_st != CLOSE_OK){
	    print_error(get_file_close_err(c_st));
	    image_destroy(&image);
	    return 4;
    }
    FILE* new_image = {0};
    o_st = open_for_write(dest, &new_image);
    if(o_st != OPEN_OK){
	    print_error(get_file_open_err(o_st));
	    image_destroy(&image);
	    return 5;
    }
    struct image n_image = {0};
    n_image = rotate(image);
    if(!n_image.data){
	    print_error("ROTATE_NO_MEM");
	    return 6;
    }
    image_destroy(&image);
    w_st =to_bmp(new_image, n_image);
    if(w_st != WRITE_OK){
	    print_error(get_BMP_write_err(w_st));
	    image_destroy(&n_image);
	    return 7;
    }
    c_st = close(new_image);
    if(c_st != CLOSE_OK){
	    print_error(get_file_close_err(c_st));
	    image_destroy(&n_image);
	    return 8;
    }
    image_destroy(&n_image);
    return 0;

}

static void print_error(const char* err){
	fprintf(stderr, "%s", err);
}
